FROM python:3 AS builder

WORKDIR /app

# Install dependencies required for building
RUN apt update && apt install -y git npm && rm -rf /var/lib/apt/lists/*
RUN npm install -g @marp-team/marp-cli

# Copy slides and generate HTML
COPY slides slides
RUN rm -f slides/presentation.html
RUN marp --html slides/presentation.md

# Final runtime stage
FROM python:3

WORKDIR /app

# Copy generated slides from builder stage
COPY --from=builder /app/slides/ slides/

# Install only necessary Python dependencies
RUN pip install Flask requests gitpython
RUN git config --global init.defaultBranch main
# Copy remaining application files
COPY . .

EXPOSE 8000
ENTRYPOINT ["python3", "server.py"]
