from flask import Flask, request, jsonify, send_from_directory, make_response
import os
import requests
import random, string
import git
TOKEN = os.environ["GOGS_TOKEN"]
GOGS_URL = os.environ["GOGS_URL"]
USERNAME = os.environ["GOGS_USER"]
PASSWORD = os.environ["GOGS_PASS"]
CLONE_DIR = "repositories"
os.makedirs(CLONE_DIR, exist_ok=True)

def create_repo(repo_name):
    try:
        response = requests.post(
            f"{GOGS_URL}/api/v1/user/repos",
            headers={"Authorization": f"token {TOKEN}"},
            json={"name": repo_name, "auto_init": False}
        )
        print(response)
        response.raise_for_status()
        return response.json()
    except Exception as e:
        print(f"Error: {str(e)}")
        return None

def repo_exists(repo_url):
    name = repo_url.split("/")[-1]
    try:
        response = requests.get(f"{GOGS_URL}/{USERNAME}/{name}")
        return response.status_code == 200
    except Exception as e:
        print(f"Error checking repo: {str(e)}")
        return False

app = Flask(__name__, static_folder='slides')

def gen_uuid():
    return ''.join(random.choices(string.ascii_letters + string.digits, k=6))

@app.route('/')
def serve_static():
    return send_from_directory(app.static_folder, "presentation.html")

@app.route('/img/<path:path>')
def serve_static_img(path):
    return send_from_directory(app.static_folder, f"img/{path}")

@app.route("/sandbox")
def create_sandbox():
    repo_url = request.cookies.get("repo")
    
    if repo_url and repo_exists(repo_url):
        return make_response("Tout est bon!")
    
    resp = create_repo(gen_uuid())
    if not resp:
        return make_response("Error creating repo", 500)
    
    url = resp["html_url"]
    clone_repo(url)
    response = make_response("Tout est bon !")
    response.set_cookie("repo", url, max_age=86400)
    
    return response


@app.route("/check1")
def check_exercice_1():
    repo_url = request.cookies.get("repo")
    if (repo_url == None):
        return "Vous n'avez pas créé de sandbox ! Revenez aux slides précédentes."
    try:
        pull_force(repo_url)
        path = get_repo_path(repo_url)
        if (has_files(path)):
            return "C'est tout bon !"
    except:
        pass
    return "Mmhh. Ce n'est pas encore bon ;-)"

@app.route("/check2-3")
def check_exercice_23():
    repo_url = request.cookies.get("repo")
    if (repo_url == None):
        return "Vous n'avez pas créé de sandbox ! Revenez aux slides précédentes."
    try:
        pull_force(repo_url)
        path = get_repo_path(repo_url)
        repo = git.Repo(path)
        last_commit = repo.head.commit  # Get the latest commit
        author_name = last_commit.author.name  # Get the author's name
        if author_name != "Magali":
            return "Parfait !!!"
    except:
        pass
    return "Mmhh. Ce n'est pas encore bon ;-)"

@app.route("/start2")
def start_exercice_2():
    repo_url = request.cookies.get("repo")
    if (repo_url == None):
        return "Vous n'avez pas créé de sandbox ! Revenez aux slides précédentes."
    try:
        print("Pulling...")
        pull_force(repo_url)
        print("Getting path")
        path = get_repo_path(repo_url)
        print(path)
        u = open(f"{path}/youpi.txt", "w")
        u.write("Coucou ;-)\n")
        u.close()
        print("written file")
        repo = git.Repo(path)
        repo.index.add(["youpi.txt"])
        repo.index.commit("toto hello from the server x)")
        print("commited")
        repo.git.push("origin", "main")
        print("pushed")
        return "C'est parti mon kiki ;-)"
    except Exception as e:
        print(e)
    return "Une erreur est survenue, pas de bol :("


@app.route("/start3")
def start_exercice_3():
    repo_url = request.cookies.get("repo")
    if (repo_url == None):
        return "Vous n'avez pas créé de sandbox ! Revenez aux slides précédentes."
    try:
        pull_force(repo_url)
        path = get_repo_path(repo_url)
        with open(f"{path}/youpi.txt", "r+") as f:
            lines = f.readlines();
            f.seek(0)
            f.write("PILOU PILOU PILOU\n")
            f.writelines(lines[1:])
            f.truncate()

        repo = git.Repo(path)
        repo.index.add(["youpi.txt"])
        repo.index.commit("mmmh this should induce some merge conflicts")
        repo.git.push("origin", "main")
        return "Heh c'est good !!!"
    except Exception as e:
        print(e)
    return "Une erreur est survenue, pas de bol :("



def get_repo_path(repo_url):
    """Extract repo name and return its local path."""
    CLONE_DIR = "repositories"
    repo_name = repo_url.rstrip('/').split('/')[-1].replace('.git', '')
    return os.path.join(CLONE_DIR, repo_name)

def clone_repo(repo_url):
    repo_path = get_repo_path(repo_url)
    auth_repo_url = repo_url.replace("http://", f"http://{USERNAME}:{PASSWORD}@")
    auth_repo_url = repo_url.replace("https://", f"https://{USERNAME}:{PASSWORD}@")
    git.Repo.clone_from(auth_repo_url, repo_path)
    repo = git.Repo(repo_path)
    with repo.config_writer() as config:
        config.set_value("user", "name", "Magali")
        config.set_value("user", "email", "info@louvainlinux.org")

def pull_force(repo_url):
    repo_path = get_repo_path(repo_url)
    repo = git.Repo(repo_path)
    origin = repo.remotes.origin
    origin.fetch()  # Fetch latest changes
    repo.git.reset("--hard", "origin/main")  # Force reset to match remote main branch

def has_files(directory):
    """Check if there is at least one file inside a directory."""
    return any(os.path.isfile(os.path.join(directory, f)) for f in os.listdir(directory))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug=True)
