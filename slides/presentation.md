---
marp: true
theme: default
class: invert
---
<style>
	kbd {
		font-size: 0.7em;
		line-height: 1.2em;
	}
</style>

# Git

**Outil de collaboration**

Louvain-li-Nux

![bg right height:50%](img/git-icon.svg)

---

## Cette présentation est

- Sous license libre GPLv2.
- Disponible en ligne : <https://git.louvainlinux.org>
- N'hésitez pas à suivre les slides en même temps que la presentation !

---

## Git, c'est quoi ?

- Un système de gestion de versions distribué.
- VCS (Version Control System), en Anglais.

---

## Mise en place de l'environnement

---

Ouvrir votre émulateur de terminal ("Terminal" sur Windows), et ensuite :

### Linux (<kbd>Ctrl</kbd><kbd>Shift</kbd><kbd>C</kbd>, <kbd>Ctrl</kbd><kbd>Shift</kbd><kbd>V</kbd>)

```sh
$ sudo apt update && sudo apt install git
```

### Windows (<kbd>Ctrl</kbd><kbd>Shift</kbd><kbd>C</kbd>, <kbd>Ctrl</kbd><kbd>Shift</kbd><kbd>V</kbd>)

- `wsl --install`
- Ouvrir une session Linux.
- Cf. Linux.

### Mac (<kbd>⌘</kbd><kbd>C</kbd>, <kbd>⌘</kbd><kbd>V</kbd>)

```sh
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" && brew install git
```

---

## WSL

- Utilisez absolument le "Windows Terminal" ! (Les autres invites de commande sur Windows ne sont pas géniales)
- Dans votre session Linux, vous pouvez taper `explorer.exe .` (sans oublier le point !) pour ouvrir le dossier contenant vos fichiers sur Linux.

---

## La ligne de commande (CLI)

Des implémentations GUI existent, *mais* :

- Moins flexibles.
- Opaques.
- Souvent fermées.
- Si vous comprenez bien le CLI, vous comprendrez mieux le GUI.

Donc $\rightarrow$ petit détour vers les bases du CLI !

---

## La Commande

```sh
$ echo "Hello world"  # commande + argument
Hello world           # sortie
```

![bg right width:100%](img/commande.png)

---

## Le Chemin (aka Path)

Lorsque vous utilisez un terminal, vous vous trouvez toujours dans un certain dossier de votre ordinateur.
Si vous exécutez la commande 
```sh
$ pwd #print working directory
/home/theo/
```
Vous saurez directement où vous vous trouvez !



---

## Le Chemin (aka Path)

Un chemin est comme un fil d'ariane, sous la forme `/dossier1/dossier2/dossier3/fichier.txt`.

Lorsque le chemin commence par un `/` cela signifie qu'il est absolu. Autrement dit, c'est un chemin complet depuis la "racine" de votre 
disque dur jusqu'a votre position.


Exemple: `/home/theo/atelier/cool.txt`

---

## Les chemins relatifs

Il est aussi possible de définir un chemin relatif a notre position actuelle.

```sh
$ pwd # "print working directory" ou "écrire le dossier de travail"
/home/theo
```

Ici par exemple, on se trouve dans `/home/theo`.

Le chemin relatif `atelier/fichier.txt` représente en fait `/home/theo/atelier/fichier.txt`

REMARQUE: Il est courant de voir la notation `./atelier/fichier.txt` qui indique aussi un chemin relatif.

---

### `pwd`

On peut trouver un chemin absolu d'un chemin relatif en ajoutant notre dossier de travail avant :

- Le fichier `tux` devient `/home/theo/tux`
- Le dossier `super` devient `/home/theo/super`
- Le fichier `super/tux2` devient `/home/theo/super/tux2`

---

### `ls` (pour LiSt)

On peut lister tous les fichiers/dossiers dans notre dossier de travail :

```sh
$ ls
tux	super/
```

Ou dans un autre dossier :

```sh
$ ls super
tux2
```

---

### `cd` (Change Directory)

On peut changer notre dossier de travail :

```sh
$ pwd
/home/theo
$ cd super
$ pwd
/home/theo/super
```

Et puis encore :

```sh
$ ls
tux2
```

---

### `cd`

On peut retourner un dossier en arrière :

```sh
$ pwd
/home/theo/super
$ cd ..
$ pwd
/home/theo
```

---

### Recap

- `pwd` : Afficher le dossier de travail.
- `ls` : Afficher le contenu d'un dossier.
- `cd` : Changer le dossier de travail.

---

## Concepts de base de git

---

### Les commits

- Contient une ou plusieurs **modifications** de fichiers.
- Ordonnés

---
#### Une modification de fichier (diff) ? 

<pre class="diff">
<span>5- Hello, <strong>World</strong></span>
<span>5+ Hello, <strong>Git !</strong></span>
</pre>

Ceci représente une modification du mot <strong>World</strong> en <strong>Git !</strong>

Chaque commit contient un ensemble de modifications comme celle ci.

<style>

.diff span:first-child {
    background-color: #ffdddd; /* Light red for removed lines */
    color: #a33;
}

.diff span:last-child {
    background-color: #ddffdd; /* Light green for added lines */
    color: #080;
}

</style>
---

### Organisations des commits



<div class="graph">
    <div class="commit">0</div>
</div>


Commençons simplement avec un seul commit. 

---

### Organisations des commits



<div class="graph">
    <div class="commit">0</div>
    <div class="commit">1</div>
</div>


Si on crée un deuxième commit, il se plaçera juste après ce premier commit.
Les commits sont organisés. Ils se mettent les uns a la suite des autres.

---

### Organisations des commits



<div class="graph">
    <div class="commit">0</div>
    <div class="commit">1</div>
    <div class="commit">2</div>
    <div class="commit">3</div>
</div>


En travaillant, vous allez construire une suite de commits, contenant toutes les modifications apportées a vos fichiers.

Je modifie → Je commit → Je modifie → Je commit

---

### Organisations des commits



<div class="graph">
    <div class="commit cwhite">0</div>
    <div class="commit">1</div>
    <div class="commit">2</div>
    <div class="commit">3</div>
</div>


Avant <span class="white">le commit initial</span>, tous les fichiers sont vides.

---


### Organisations des commits



<div class="graph">
    <div class="commit cwhite">0</div>
    <div class="commit">1</div>
    <div class="commit">2</div>
    <div class="commit csalmon">3</div>
</div>



Avant <span class="white">le commit initial</span>, tous les fichiers sont vides.

Le <span class="salmon">HEAD</span> représente l'endroit ou vous vous trouvez actuellement sur l'historique. Le contenu des fichiers que vous avez sur votre ordinateur est défini par les diffs des commits parents (appliqués chronologiquement).


Donc ici nous verrons les modifications apportées par `0+1+2+3`

---
### Organisations des commits



<div class="graph">
    <div class="commit cwhite">0</div>
    <div class="commit">1</div>
    <div class="commit csalmon">2</div>
    <div class="commit">3</div>
</div>




Si l'on change <span class="salmon">HEAD</span> vers le commit `2`, on pourra voir l'état des fichiers sans les modifications apportées par `3`. (Seulement `0+1+2`)

Il est donc possible avec git de retourner dans le passé, pour voir l'état des fichiers avant X ou Y modification.

---
### Les branches


Il manque une dernière pièce avant de pouvoir vraiment utiliser Git, et ce sont les branches.

---


### Les branches

Une suite de commits ne doit pas forcément être linéaire !

<!-- Would like to make this pure html to make it easy to change -->
![width:700px](img/branches-tree.png)

Chaque bifurcation démarre une nouvelle branche.

Dans notre cas, <span class="salmon">HEAD</span> pointe vers `6`, nos fichiers
contiendront les diffs de 1 + 2 + 5 + 6.



---

### Le repo ("dépôt" en 🇫🇷)

- Endroit où notre code est stocké, ainsi que l'historique des versions et toutes les données relatives à `git`.
- Généralement, un projet == un repo.


---

# Des questions ?
Yay ! Si vous avez compris tout ça, on peut continuer.

---


### Configuration de pour la suite de cet atelier git

```sh
git config --global user.name "Votre nom"
git config --global user.email "votre@email.com"
git config --global pull.rebase false
git config --global push.autoSetupRemote true
git config --global init.defaultBranch main
```

---

Créez votre repo :

```sh
$ mkdir cool-repo
$ cd cool-repo
$ git init
Dépôt Git vide initialisé dans /home/###/cool-repo/.git/
```

Un repo vide à été créé avec succès ! Et il ne contient donc aucun commit !)

---

Créons maintenant un fichier, appelé "README.md" (ou autrement, comme vous voulez !)
```sh
$ touch README.md
```

---

Ensuite, écrivez ce que vous voulez dedans.
(Ouvrez le avec bloc notes, ça marche très bien)

---

Nous allons maintenant "enregistrer" cette modification dans
la repo git.

---
## Le stage
<div class="repo-box">
  <div class="repo-container">
    <div class="box">
      <div class="changes">
        + &nbsp;  Votre Modif </div>
    </div>
    <p>Working Repo</p>
  </div>
  <div class="invisible-box">
    <!-- &rarr; -->
  </div>
  <div class="repo-container">
    <div class="box"></div>
    <p>Local Git Repo</p>
  </div>
  </div>
  <br>
<style>
.repo-container{
  min-width: 300px;
  text-align : center;
}
.invisible-box {
  height: 180px;
  width:340px;
  text-align: center;
}
.box{
  height: 180px;
  width: 300px;
  border: 3px solid white;
  padding: 20px;
  gap: 20px;
  border-radius: 10%;
}
.repo-box {
  display: flex;
  flex-direction: row;
  margins: auto;
  gap: 20px;
  top: 0;
}
.diapo{
  position : relative;
}
.changes{
  border: 3px solid #57e559;
  border-radius: 10px;
  color: #57e559;
  display: flex;
  flex-direction : row;
  align-text: center;
  align-items: center;
  padding: 10px;
  font-size: 25px;
  width: 60%:
  gap: 40px;
}
.multiple-changes{
  border: 3px solid #57e559;
  border-radius: 10px;
  color: #57e559;
  display: flex;
  flex-direction : row;
  align-text: center;
  align-items: center;
  font-size: 20px;
  width: 80%;
  padding : 10px;
  gap: 20px;
}
.arrow-box{
  position : relative;
  height: 100px;
  text-align: center;
  color: #6aabfc;
}
.stage-box{
  border-style: dotted;
  display: flex;
  flex-direction : column;
}
.arrow{
  position: absolute;
  bottom: 0;
  left: 270px;
}
.committed{
  border-radius: 100%;
  height: 100px;
  width: 100px;
  border: 3px solid white;
  color: white;
  
}
</style>

---

## Le stage
<div class="diapo">
<div class="repo-box">
  <div class="repo-container">
  <div class="box">
    <div class="changes">
      + &nbsp;  Votre Modif </div>
  </div>
    <p>Working Repo</p>
  </div>
  <div class="repo-container">
    <div class="box stage-box">
    </div>
    Stage
  </div>
  <div class="repo-container">
    <div class="box"></div>
    <p>Local Git Repo</p>
  </div>
  </div>
  
  <br>
  <div class = "text">
  Permet de "préparer" ces commits.
  C'est un espace de travail pour les changements.
  </div>
  </div>

---

## Le stage 
  <div class="repo-box">
    <div class="repo-container">
    <div class="box">
    </div>
      <p>Working Repo</p>
    </div>
    <div class="repo-container">
      <div class="box stage-box">
      <div class="changes">
        <div class="plus">+</div>
        <div>Votre Modif</div> </div>
      </div>
      Stage
    </div>
    <div class="repo-container">
      <div class="box"></div>
      <p>Local Git Repo</p>
    </div>
  </div>
    <br>

```sh
$ git add chemin/vers/mon/fichier
```
  
---

## Le stage 
  <div class="repo-box">
    <div class="repo-container">
    <div class="box">
    </div>
      <p>Working Repo</p>
    </div>
    <div class="repo-container">
      <div class="box stage-box">
      <div class="changes">
        <div class="plus">+</div>
        <div>Votre Modif</div> </div>
      </div>
      Stage
    </div>
    <div class="repo-container">
      <div class="box"></div>
      <p>Local Git Repo</p>
    </div>
    </div>

Vous pouvez visualiser la stage avec: 
```sh
$ git diff --staged 
```

Et vous pouvez visualiser ce que vous pouvez ajouter à la stage avec :
```sh
$ git diff 
```    
---

## Le stage 
  <div class="repo-box">
    <div class="repo-container">
    <div class="box">
      <div class="changes">
        <div class="plus">+</div>
        <div>Votre Modif</div> </div>
    </div>
      <p>Working Repo</p>
    </div>
    <div class="repo-container">
      <div class="box stage-box">
      </div>
      Stage
    </div>
    <div class="repo-container">
      <div class="box"></div>
      <p>Local Git Repo</p>
    </div>
    </div>
    <br>

Vous pouvez nettoyer la stage avec: 
```sh
$ git reset
```
---
## Le stage 
  <div class="repo-box">
    <div class="repo-container">
    <div class="box">
    </div>
      <p>Working Repo</p>
    </div>
    <div class="repo-container">
      <div class="box stage-box">
      </div>
      Stage
    </div>
    <div class="repo-container">
      <div class="box"></div>
      <p>Local Git Repo</p>
    </div>
    </div>
    <br>

Vous pouvez effacer tous vos changements avec: 
```sh
$ git restore
```
---


### Statut global 


```sh
$ git status
On branch main
Your branch is up to date with 'origin/main'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	modified:   fichier-modifié-et-staged

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   fichier-modifié-mais-pas-stageed

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	nouveau-fichier

no changes added to commit (use "git add" and/or "git commit -a")
```
---
## Le stage 
  <div class="repo-box">
    <div class="repo-container">
    <div class="box">
      <div class="multiple-changes">
        <div>+ Une modif</div> </div>
    </div>
      <p>Working Repo</p>
    </div>
    <div class="repo-container">
      <div class="box stage-box">
        <div class="multiple-changes">
          <div>+ Une autre modif</div> </div>
        <div class="multiple-changes">
          <div>+ Encore une autre modif !!</div> </div>
      </div>
      Stage
    </div>
    <div class="repo-container">
      <div class="box"></div>
      <p>Local Git Repo</p>
    </div>
    </div>
    <br>

Bref, une fois que tous vos changements sont sur la stage...
(il peut y en avoir autant que vous voulez)

---

## Le stage 
  <div class="repo-box">
    <div class="repo-container">
    <div class="box">
      <div class="multiple-changes">
        <div>+ Une modif</div> </div>
    </div>
      <p>Working Repo</p>
    </div>
    <div class="repo-container">
      <div class="box stage-box">
      </div>
      Stage
    </div>
    <div class="repo-container">
      <div class="box">
        <div class="changes committed blue">
          <div></div>
        </div>
      </div>
      <p>Local Git Repo</p>
    </div>

  </div>
    <br>

Vous pouvez commit vos changements.

```sh
$ git commit -m "Modification de README.md"
```
---
## Le stage
<div class="repo-box-what">
  <div class="commit-box">
    <div class="graph">
        <div class="commit">0</div>
        <div class="commit">1</div>
        <div class="commit">2</div>
        <div class="commit blue"></p></div>
    </div>
  </div>
  Local Git Repo
</div>
<br>
ceux-ci seront ajoutés aux précédents

<style>
.repo-box-what{
  display: flex;
  flex-direction : column;
  gap: 20px; 
  text-align: center;
}
.commit-box{
  display: flex;
  flex-direction: column;
  border: 3px solid white;
  border-radius: 20px;
}
p.small{
  font-size: 20px;
}
.blue{
background-color: #305ead}
.green{
background-color: #4ee05d}
</style>
---

### Le log

- Au final, notre code est une superposition de ces changements (commits) en ordre chronologique.

```sh
$ git log
commit 3e54... (HEAD -> main, origin/main, origin/HEAD)
Author: Tux <info@louvainlinux.org>
Date:   Thu Feb 29 14:06:18 2024 +0100

    Dernier commit

commit de92ddfcc4f6c9e06dfc148f681e35aeca32ac01
Author: Tux <info@louvainlinux.org>
Date:   Thu Feb 29 14:05:25 2024 +0100

    Premier commit
```


---
## La Repo Remote

- La version de la repo git qui est stockée sur un serveur.
- C'est la que tous les changements sont mis en commun.
---
## Le Repo Remote

<div class="repo-box-what">
  <div class="">
    <div class="graph">
        <div> Repo Locale &nbsp;</div>
        <div class="commit">0</div>
        <div class="commit">1</div>
        <div class="commit">2</div>
        <div class="commit blue"></div>
    </div>
  </div>
</div>
<div class="repo-box-what">
  <div class="">
    <div class="graph">
        <div> Repo Remote </div>
        <div class="commit">0</div>
        <div class="commit">1</div>
        <div class="commit">2</div>
    </div>
  </div>
</div>

Mais comment "envoyer" nos changements ? 

---

## Push 

<div class="repo-box-what">
  <div class="">
    <div class="graph">
        <div> Repo Locale &nbsp;</div>
        <div class="commit">0</div>
        <div class="commit">1</div>
        <div class="commit">2</div>
        <div class="commit blue"></div>
    </div>
  </div>
</div>
<div class="repo-box-what">
  <div class="">
    <div class="graph">
        <div> Repo Remote </div>
        <div class="commit">0</div>
        <div class="commit">1</div>
        <div class="commit">2</div>
        <div class="commit blue"></div>
    </div>
  </div>
</div>


```sh
$ git push
```
---


<div class="repo-box-what">
  <div class="">
    <div class="graph">
        <div> Repo Locale &nbsp;</div>
        <div class="commit">0</div>
        <div class="commit">1</div>
        <div class="commit">2</div>
        <div class="commit blue"></div>
    </div>
  </div>
</div>
<div class="repo-box-what">
  <div class="">
    <div class="graph">
        <div> Repo Remote </div>
        <div class="commit">0</div>
        <div class="commit">1</div>
        <div class="commit">2</div>
        <div class="commit blue"></div>
        <div class="commit green"></div>
    </div>
  </div>
</div>

Et si maintenant quelqu'un d'autre a commit des changements entre temps ? 

---


<div class="repo-box-what">
  <div class="">
    <div class="graph">
        <div> Repo Locale &nbsp;</div>
        <div class="commit">0</div>
        <div class="commit">1</div>
        <div class="commit">2</div>
        <div class="commit blue"></div>
        <div class="commit green"></div>
    </div>
  </div>
</div>
<div class="repo-box-what">
  <div class="">
    <div class="graph">
        <div> Repo Remote </div>
        <div class="commit">0</div>
        <div class="commit">1</div>
        <div class="commit">2</div>
        <div class="commit blue"></div>
        <div class="commit green"></div>
    </div>
  </div>
</div>

```sh
$ git pull
```
---
## Recap

Ajouter vos changements a la stage

```sh
$ git add -A
```
Commit ces changements vers la repo git locale

```sh
$ git commit -m "j'ai fais un changement"
```

Ajouter ces changements à la repo git remote

```sh
$ git push
```
Récuperer les changements remote fais par d'autre contributeur

```sh
$ git pull
```
---
## Création de votre sandbox

<script src="https://cdn.jsdelivr.net/gh/bigskysoftware/fixi@0.6.4/fixi.js"
        crossorigin="anonymous"
        integrity="sha256-AsOVx1uVS24N8IuJrLq8A4yxp4n2lWxLkO+7KKKsx8Q="></script>

<span class="repo"></span>
<div>
  <p id="status">Status: Pas encore OK</p>
  <button fx-action="/sandbox" fx-target="#status" fx-swap="innerHTML">Créer une sandbox</button>
</div>
<style>
button {
  background-color: #e67e22;
  border: none;
  padding: 10px 15px;
  border-radius: 5px;
  transition: all .2s;
}
button:hover {
  background-color: #d35400;
  cursor: pointer;
}
</style>

---


### Recap (1/2)

- Stage : Endroit où on prépare les changements à être ajoutés à un commit.
- Commit : L'acte de prendre une "photo" des changements sur le stage et mettre un nom dessus.
- Log : La liste de tous les commits.
- Le local repo : La version locale de votre projet
- Le remote repo : La version en ligne de votre projet

---


### Recap (2/2)

- Stage : Endroit où on prépare les changements à être ajoutés à un commit.
- Commit : L'acte de prendre une "photo" des changements sur le stage et mettre un nom dessus.
- Log : La liste de tous les commits.

---

## Un exemple concret !

*Démo en live de ce que je viens d'expliquer.*

---

## Exercices git

Assurez-vous de bien être dans le dossier de votre repo :

```sh
$ pwd
/home/tux/repo-##
```

---

### Setup de la remote

Afin de pouvoir travailler en collaboration, il va falloir ajouter une remote !

<pre>
<code>
$ git remote add origin <span class="repo"></span>
</code>
</pre>

---

### Exercice 1 : Les bases

- Modifier le fichier `README.md`
- "Stage" les changements : `git add README.md`
- "Commit" les changements : `git commit -m "descriptif de mes changements"`
- "Push" les changements : `git push origin main`

<div>
  <p id="status2">Status: Pas encore OK</p>
  <button class="check" fx-action="/check1" fx-target="#status2" fx-swap="innerHTML">Vérifie si tu as réussi !</button>
</div>


---

## Exercices sur la collaboration

### Merges, conflits, résolutions

---

### Exercice 2 : Changements sur le remote

<div>
  <p id="status3">Status: Pas encore OK</p>
  <button fx-action="/start2" fx-target="#status3" fx-swap="innerHTML">Lancer l'exercice 2 !</button>
</div>


---

### Exercice 2 : Changements sur le remote

*Contexte* : quelqu'un a push des changements après toi.
Essayez de faire un nouveau commit.

---

### Exercice 2 : Changements sur le remote

```sh
$ git push
[main 7c64a36] test
To https://git.louvainlinux.org/repo-##
 ! [rejected]        main -> main (fetch first)
error: failed to push some refs to 'https://git.louvainlinux.org/repo-##'
hint: Updates were rejected because the remote contains work that you do not
hint: have locally. This is usually caused by another repository pushing to
hint: the same ref. If you want to integrate the remote changes, use
hint: 'git pull' before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

---

### Exercice 2 : Changements sur le remote

- "Pull" les changements du remote : `git pull origin main`
- Vous verrez un nouveau fichier en faisant `ls`
- Faites un `git log` et vous verrez qu'un commit a été ajouté avant le votre.
- "Push" votre commit et ça marchera : `git push`

<div>
  <p id="status4">Status: Pas encore OK</p>
  <button class="check" fx-action="/check2-3" fx-target="#status4" fx-swap="innerHTML">Check exercice 2</button>
</div>

---

### Exercice 3 : Conflits de merge


<div>
  <p id="status5">Status: Pas encore OK</p>
  <button fx-action="/start3" fx-target="#status5" fx-swap="innerHTML">Lancer l'exercice 3 !</button>
</div>



---

### Exercice 3 : Conflits de merge

*Contexte* : quelqu'un a push un commit qui a modifié quelque chose qu'un commit à vous a aussi modifié.

---

### Exercice 3 : Conflits de merge

- Changez la première ligne de `youpi.txt` vers ce que vous voulez.
- Committez ce changement.
- Essayez de "push" `git push origin main`
=> Cela ne marche pas 
- "Pull" les derniers changements du remote : `git pull origin main`

```sh
$ git pull
Auto-merging youpi.txt
CONFLICT (content): Merge conflict in youpi.txt
Automatic merge failed; fix conflicts and then commit the result.
```

---

### Exercice 3 : Conflits de merge

- Ouvrez `youpi.txt` et gardez ce que vous voulez :

```md
<<<<<<< HEAD
# Mon nouveau titre
=======
PILOU PILOU
>>>>>>> main
```

---

### Exercice 3 : Conflits de merge

- Committez la resolution de merge :

```sh
git add pilou.txt
git commit -m "merge: Merge from main"
git push origin main
```

<div>
  <p id="status6">Status: Pas encore OK</p>
  <button class="check" fx-action="/check2-3" fx-target="#status6" fx-swap="innerHTML">Check exercice 3</button>
</div>



---

## Travail de groupe

Pour cette partie de la presentation, mettez vous en groupes de 2.
Choisissez le repo que vous préférez - vous allez à présent tous les deux travailler dessus.
Votre binôme va devoir cloner votre repo (ou vous devrez cloner le sien, choisissez)



<pre>
<code>
$ git clone <span class="repo"></span>

</code>
</pre>
---

### Exercice 4 : Générer un merge conflict

Faites en sorte qu'il y ait un merge conflict :

- Modifiez tous les deux la même ligne de code.
- Faites tous les deux un commit et essayez de push.
- L'un d'entre vous devra pull et resolver le merge conflict.

Ensuite, inversez les rôles !

---

## Branches

- Permettent que chacun travaille "dans son coin".
- Les choses sont mises en commun tout à la fois plutôt qu'après chaque commit.
- Permettent de tester des changements avant de les faire passer en production.
- La branche principale s'appelle `main`.

---

### Lister les branches

On peut voir une liste des branches :

```sh
$ git branch
* main
  branch2
```

L'étoile indique sur quelle branche on est.

---

### Changer de branche

On peut changer notre branche courante :

```sh
$ git checkout branch2
$ git branch
  main
* branch2
```

---

### Nouvelle branche

On peut créer une nouvelle branche :

```sh
$ git checkout -b branch3
$ git branch
  main
  branch2
* branch3
```

---

### Visualisation des branches

On peut visualiser les branches :

```sh
$ git log --oneline --decorate --graph --all

# C'est un peu trop long pour taper ici,
# regarder plutôt la jolie image à droite.
```

![bg right height:70%](img/branches.png)

---

### Merge d'une branche

On peut merge d'une branche :

```sh
$ git checkout main
$ git merge branch2
$ git merge branch3
```

Et les éffacer ensuite :

```sh
$ git branch -d branch2
$ git branch -d branch3
```

---

### Branches sur remote

On peut push nos branches pour les utiliser autre part :

```sh
$ git push origin branch
```

---

### Exercice 5 : Branches

- Toujours en groupes de deux.
- Créez chacun une branche.
- Faites quelques commits sur vos branches.
- Push vos changements.
- Pull les changements de l'autre.
- Merge la branche de l'autre (et resolvez les conflits s'il y en a).
- Push le merge.

---

## Étiquette git

---

### Ne pas push des gros blobs binaires

- Git est fait primairement pour du texte.
- Éviter de push des gros fichiers s'ils ne constituent pas la "source" du projet (i.e. si l'utilisateur sait le générer lui-même, ça n'a pas lieu d'être sur git).

---

### Solution : `.gitignore`

Fichier à la racine du repo qui contient une liste de chemins à ne pas être commit :

```sh
je/veux/surtout/pas/commit/ce/dossier

# "*" veut dire "main.o", mais aussi "dfkjghdkfg.o"
# ou encore "sedidhoi.o"

*.o
```

---

### Avoir le diff le plus petit

- Ne pas modifier des choses qui ne sont pas pertinentes.
- **Exemple** : Changer le formattage d'un fichier auquel on a autrement pas touché, même s'il est incorrect.
- **But** : Avoir le diff le plus petit. Un petit diff est plus facile à review et à "blâmer" (on verra ça plus tard).

---

### Pas de "trailing whitespaces"

- Ne pas laisser des espaces blancs à la fin des lignes.
- `git diff` affichera même en rouge pour pas qu'on passe à côté :

![bg right height:25%](img/trailing-whitespace.png)

---

### Avoir un newline (`\n`) à la dernière ligne

Sur VS Code, ajouter dans `settings.json` :

```json
{
	"files.insertFinalNewline": true
}
```

Pourquoi ce n'est pas activé par défaut ? Un mystère...
Important car sinon un diff qui ajoute une ligne en modifiera deux, vu qu'il faut aussi ajouter une newline à la précédente.

---

## Contribution à un repo Open Source

Maintenant on va mettre ce que vous avez appris en pratique !

---

### Créer un compte GitHub

- Allez sur <https://github.com>.
- Débrouillez-vous.

---

### Mettre en place les clefs SSH (authentification)

```sh
$ ssh-keygen -t ed25519 -P ""
# Appuyez sur ENTER
$ cat ~/.ssh/id_ed25519.pub
```

Copier la sortie de cette commande.

---

### Ajouter la clef SSH sur GitHub

Une fois dans les paramètres, allez sur "SSH and GPG keys".

![bg right height:70%](img/ssh.png)

---

### Ajouter la clef SSH sur GitHub

Appuyez sur "New SSH key".

![bg right height:70%](img/new-ssh-key.png)

---

### Ajouter la clef SSH sur GitHub

Collez votre clef SSH public.
Soyez sûr de bien copier `id_ed25519.pub` et pas `id_ed25519` !

![bg right height:70%](img/add-ssh-key.png)

---

### Créer un repo sur git

![height:70% bg](img/new-repo.png)

---

![height:100% bg](img/create-new-actually.png)

---

![height:90% bg](img/copy-addr.png)

---

### Ajouter un second remote

```sh
$ git remote
origin
$ git remote add github ssh://git@github.com/tux/mon-repo
$ git remote
origin
github
$ git push github
```

---

## Un exemple concret !

*Démo en live de ce que je viens d'expliquer.*

---

## Contribuer à un repo Open Source

On va utiliser <https://github.com/firstcontributions/first-contributions>, qui est un projet fait exprès pour introduire les gens aux contributions Open Source.

---

### Fork

![height:70% bg](img/fork.png)

---

### Faire ses changements

```sh
$ git clone ssh://git@github.com/tux/first-contributions
$ cd first-contributions
```

Ajoutez votre nom ou ce que vous voulez au fichier `README.md`.

```sh
$ git add README.md
$ git commit -m "mes changements"
$ git push
```

---

### Ouvrir une PR

![](img/open-pr.png)

---

## Un exemple concret !

*Démo en live de ce que je viens d'expliquer.*

---

## Features avancées

- `git add -p`
- Tags.
- Les hooks : <pre-commit.com>.
- `git commit --amend`
- `git blame`
- `git reset HEAD~1`

---

# Merci pour votre attention !

Les slides sont disponibles en ligne :

<https://slides.git.louvainlinux.org>



<style>
    .graph {
        display: flex;
        align-items: center;
        margin: 5%;
    }
    .commit {
        width: 80px;
        height: 80px;
        border: 2px solid white;
        border-radius: 50%;
        margin: 0 40px;
        position: relative;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 25px;
        
     }
    .end  {
      margin-left: 45px;
    }


    .commit:not(:last-child)::after {
        content: "";
        position: absolute;
        top: 50%;
        left: 100%;
        width: 105%;
        height: 2px;
        background: white;
        transform: translateY(-50%);
    }
    .salmon {
      background-color: salmon;
      padding: 5px;
      border-radius: 5px;
    }
    .white {
      background-color: white;
      color: black;
      padding: 5px;
      border-radius: 5px;
    }

    .csalmon {
      background-color: salmon;
    }
    .cwhite {
      color: black;
      background-color: white;
    }

    .repo {
      background-color: #34495e;
      padding: 5px;
      border-radius: 5px;
    }
    .repo:empty {
      display: none;
    }


    .check {
      background-color: #27ae60;
    }

</style>



<script>
function getCookie(name) {
    let cookies = document.cookie.split('; ');
    for (let cookie of cookies) {
        let [key, value] = cookie.split('=');
        if (key === name) {
            return value;
        }
    }
    return null; // Return null if cookie not found
}


document.addEventListener("fx:after", (evt) => {
  update()
})

function update() {
  let places = document.getElementsByClassName("repo")
  for (place of places) {
    place.innerHTML = getCookie("repo")
  }
}

addEventListener("DOMContentLoaded", (event) => {
  let places = document.getElementsByClassName("repo")
  for (place of places) {
    place.innerHTML = "Tu dois créer une sandbox :)"
  }

  
});



</script>
